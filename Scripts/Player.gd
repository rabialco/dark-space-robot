extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)
const sprite = Sprite

var velocity = Vector2()
var stand_sprite = load('res://Asset/kenney_tooncharacters1/Robot/PNG/Poses HD/character_robot_idle.png')
var jump_sprite = load('res://Asset/kenney_tooncharacters1/Robot/PNG/Poses HD/character_robot_hang.png')
var double_jump_sprite = load('res://Assets/kenney_platformercharacters/PNG/Soldier/Poses/soldier_climb1.png')
var pressed = false
var pressed_counter = 0
var expand_light = false
var timer = null
var expand_label = "EXPAND!"

func get_input():
	var animation = "idle"
	velocity.x = 0
	
	if Input.is_action_just_pressed('ui_e'):
		timer = Timer.new()
		timer.set_one_shot(true)
		timer.set_wait_time(2.0)
		timer.connect("timeout",self,"finish_expand")
		add_child(timer)
		if expand_light == false:
#			$RichTextLabel.set_text(str(expand_label))
			$RichTextLabel.show()
			$RichTextLabel/Timer.start()
			timer.start()
			$Light2D.set_texture_scale(14.0)
			expand_light = true
			
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		pressed_counter += 1
		velocity.y = jump_speed
		$jump_sound.stream = load('res://Asset/sound/whoosh_sound.wav')
		$jump_sound.play()
		
		
	if is_on_floor() == false :
		animation = "jump"
		
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		animation = "jalan_kanan"

	if Input.is_action_pressed('ui_right') and Input.is_action_pressed('ui_up'):
		#velocity.x += speed/4
		animation = "lompat_kanan"
	
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		animation = "jalan_kiri"
	
	if Input.is_action_pressed('ui_left') and Input.is_action_pressed('ui_up'):
		animation = "lompat_kiri"
	
	if is_on_floor() == false and Input.is_action_just_pressed('ui_up') and pressed_counter == 1:
		pressed_counter += 1
		velocity.y = jump_speed
		$jump_sound.stream = load('res://Asset/sound/whoosh_sound.wav')
		$jump_sound.play()
			
	if $AnimatedSprite.animation != animation :
		$AnimatedSprite.play(animation)

func finish_expand():
	$RichTextLabel.hide()
	$Light2D.set_texture_scale(7.0)
	expand_light = false

func _physics_process(delta):
	if is_on_floor() and Input.is_action_just_pressed('ui_right') == false and Input.is_action_just_pressed('ui_left') == false:
		$Sprite.texture = stand_sprite
		pressed_counter = 0
	
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
