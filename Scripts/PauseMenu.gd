extends CanvasLayer

func _ready():
	set_visible(false)

func _input(event):
	var sceneName = get_tree().get_current_scene().get_name()
	if sceneName == "Level1" or sceneName == "Level2":
		if event.is_action_pressed("ui_cancel"):
			set_visible(!get_tree().paused)
			get_tree().paused = !get_tree().paused


func _on_Resume_pressed():
	get_tree().paused = false
	set_visible(false)
	
func set_visible(is_visible):
	for node in get_children():
		node.visible = is_visible


func _on_Back_to_Menu_pressed():
	set_visible(false)
	get_tree().paused = !get_tree().paused
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))
