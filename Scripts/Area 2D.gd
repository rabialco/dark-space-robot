extends Area2D

export (String) var sceneName = "Level1"

func _on_Area_Trigger_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
		if current_scene == sceneName :
			Global.lives = floor(Global.lives * 0.5)
		if (Global.lives == 0):
			Global.lives = 100
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		else:
			get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
