extends RichTextLabel

var s = 2
var label = "Expand Time : "
var second = " s"

func _process(delta):
	if s == 0:
		s = 2
		self.hide()
	set_text(str(label) + str(s) + str(second))


func _on_Timer_timeout():
	s -= 1
